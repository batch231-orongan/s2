console.log("Hello World!");

// Without the use of objects, our students before would be organize as follows. If we are to record additional information about them.

// sphagetti code - code is not organize enough that it becomes hard to work with.
// encapsulation - organize related information (properties) and behavior (methods) to belong to a single entity.


//create student one
// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

// Encapsulate the following information into 4 student objects using object literals.

let studentOne = {
	name: "Tony",
	email: "starksindustries@mail.com",
	grades: [89, 84, 78, 88],

	// add functionalities availabel to a student as object methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`)
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
		})	
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	// mini-activity
	// create a method that will return true if average grade is >= 85, false otherwise.
	// name method willPass

	willPass(){
		if(studentOne.computeAve() >= 85){
			return true
		}else{
			return false
		}
	},

	// miss tine solution
	/*
		// you can call method inside an object
		willPass(){
			return this.computeAve() >= 85 ? true : false
		}
		
		ternarry operation syntax: condition ? value if condition is true : value if condition is false
	*/

	// method that returns true if the student has passed and their average is equal to 90, otherwise false.

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}

};
	console.log(`student one's name is ${studentOne.name}`);
	console.log(this); // result: global window object


// mini-activity -encapsulate the propepr properties and methods to studentTwo, StudentThree and StudentFour

let studentTwo = {
	name: "Peter",
	email: "spideyman@mail.com",
	grades: [78, 82, 79, 85],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`)
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
		})	
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}

};

let studentThree = {
	name: "Wanda",
	email: "scarlettMaximoff@mail.com",
	grades: [87, 89, 91, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`)
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
		})	
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}
};

let studentFour = {
	name: "Steve",
	email: "captainRogers@mail.com",
	grades: [91, 89, 92, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`)
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
		})	
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}
};




// Mini-Quiz:
/*
	1. What is the term given to unorganized code that's very hard to work with?
		Ans: Spagehetti code
   

   2. How are object literals written in JS?
   		ans: {} / curly braces
  

   3. What do you call the concept of organizing information and functionality to belong to an object?
   		Ans: OOP (Object-Oriented Programming)
   		correct ans: Encapsulation

   4. If student1 has a method named enroll(), how would you invoke it?
   		Ans: studentOne.enroll();
  

   5. True or False: Objects can have objects as properties.
  		Ans: True

   6. What does the this keyword refer to if used in an arrow function method?
   		Ans: properties within the object
   		correct ans: global window object

   7. True or False: A method can have no parameters and still work.
		Ans: True

   8. True or False: Arrays can have objects as elements.
   		Ans: True

   9. True or False: Arrays are objects.
   		Ans: True

   10. True or False: Objects can have arrays as properties.
		Ans: True

*/

const classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour], 

	countHonorStudent(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				result++;
			}
		})
		return result;
	},

	// Function Coding Activity
	
	// 1. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

	honorsPercentage(){
		return((this.countHonorStudent()/this.students.length)*100);
	},

	// 2. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

	retrieveHonorStudentInfo(){
		let result = [];
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				result.push( {
					"email": student.email, 
					"aveGrade": student.computeAve()
				});
			}
		})
		return result;
	},

	// 3. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

	sortHonorStudentsByGradeDesc(){
		let result = this.retrieveHonorStudentInfo().sort((a,b) => (b.aveGrade) - (a.aveGrade));

		return result;
	}	
}

